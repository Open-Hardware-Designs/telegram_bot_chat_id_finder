# Telegram bot chat ID finder

***Find out the chat id without inviting random chat bots into your group.***

1. Setup Python env.
```bash
python3 -m venv env
```
2. Upgrade pip
```bash
pip install --upgrade pip
```
3. Install telegram bot libraries
```bash
pip install python-telegram-bot
```
4. Make sure your bot is in your channel
5. Run script with your token
```bash
python3 telegram_chat_id.py --token abcd:123456798
```
6. Message anything in the chat and bot will publish your chat ID
7. Close the script ```Ctrl+C```

![](chat-id.gif)
